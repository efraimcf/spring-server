package br.com.sysk.springServer;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

/**
 * Initialization class for Application
 * 
 * @author Efraim Coutinho Ferreira
 * @version 1.0
 */
@SpringBootApplication
public class ApplicationContext extends SpringBootServletInitializer {

	public static void main( final String[] args ) {
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
		SpringApplication.run( ApplicationContext.class, args );
	}
	
	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
	 
	   return (container -> {
	        ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/error?code=" + HttpStatus.UNAUTHORIZED.value());
	        ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/error?code=" +  + HttpStatus.NOT_FOUND.value());
	        ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error?code=500");
	 
	        container.addErrorPages(error401Page, error404Page, error500Page);
	   });
	}
}