package br.com.sysk.springServer.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Classe responsável pelo auxílio ao sistema de auditoria do sistema,
 * recuperando o usuário ativo que está efetuando as atualizações na base
 * de dados
 * 
 * @author Efraim Coutinho
 *
 */
@Component
public class AuditorConfig  implements AuditorAware<String> {

	@Override
	public String getCurrentAuditor() {
		return SecurityContextHolder.getContext().getAuthentication() == null ? "Annonimous" :
			SecurityContextHolder.getContext().getAuthentication().getName();
	}
}
