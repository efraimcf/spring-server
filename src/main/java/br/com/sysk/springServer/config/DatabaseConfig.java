package br.com.sysk.springServer.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * Configuração do Database
 * 
 * Utiliza o c3p0 para gerenciar o pool de conexões e a reconexão com o banco em caso de
 * fechamento da conexão por idle
 * 
 * @author Efraim Coutinho
 *
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorConfig")
public class DatabaseConfig {

	private static final String PACKAGE_TO_SCAN = "br.com.sysk.springServer.model";
	
	@Value("${db.c3p0.minPoolSize}")
	private Integer minPoolSize;

	@Value("${db.c3p0.maxPoolSize}")
	private Integer maxPoolSize;

	@Value("${db.c3p0.maxIdleTime}")
	private Integer maxIdleTime;

	@Value("${db.url}")
	private String dbUrl;
	
	@Value("${db.username}")
	private String username;
	
	@Value("${db.password}")
	private String password;

	@Value("${db.driver}")
	private String driver;
	
	@Value("${db.dialect}")
	private String dialect;
	
	@Value("${db.showSql}")
	private String showSQL;
	
	@Value("${db.ddl-auto}")
	private String hbm2ddl;

	@Bean
	public ComboPooledDataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setMinPoolSize(minPoolSize);
		dataSource.setMaxPoolSize(maxPoolSize);
		dataSource.setMaxIdleTime(maxIdleTime);
		dataSource.setJdbcUrl(dbUrl);
		dataSource.setPassword(password);
		dataSource.setUser(username);
		dataSource.setDriverClass(driver);

        return dataSource;
	}
	
	@Bean
    public LocalSessionFactoryBean sessionFactory() throws PropertyVetoException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(PACKAGE_TO_SCAN);
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", dialect);
        hibernateProperties.put("hibernate.show_sql", showSQL.equals("true"));
        hibernateProperties.put("hibernate.hbm2ddl.auto", hbm2ddl);
        sessionFactoryBean.setHibernateProperties(hibernateProperties);

        return sessionFactoryBean;
    }
	
	@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource());
        entityManagerFactory.setPackagesToScan(PACKAGE_TO_SCAN);
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        Properties additionalProperties = new Properties();
        additionalProperties.put("hibernate.dialect", dialect);
        additionalProperties.put("hibernate.show_sql", showSQL);
        additionalProperties.put("hibernate.hbm2ddl.auto", hbm2ddl);
        entityManagerFactory.setJpaProperties(additionalProperties);

        return entityManagerFactory;
    }
	
	@Bean
    public JpaTransactionManager transactionManager() throws PropertyVetoException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        
        return transactionManager;
    }
	
	@Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
