package br.com.sysk.springServer.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configuração do Spring Security utilizando Basic Authentication para 
 * as chamadas REST
 * 
 * @author Efraim Coutinho
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter  {

	private static final String ROLE = "USER";
	
	@Value("${server.username}")
	private String username;
	
	@Value("${server.password}")
	private String password;
	
	/**
	 * Libera todas as requisições do método Options, mas valida usuário e senha
	 * para todas as outras requisições.
	 */
	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/healthcheck**")
					.permitAll()
		    	.antMatchers("/**")
					.authenticated()
				.and()
			.httpBasic()
				.and()
			.csrf()
	    		.disable();
	}
	
	/**
	 * Authenticação feita em memória. Caso a authenticação seja feita com base em um
	 * usuário registrado no banco, um AuthenticationProvider deve ser implementado.
	 */
	@Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        authManagerBuilder.inMemoryAuthentication()
                .withUser(username).password(password).roles(ROLE);
    }
}
