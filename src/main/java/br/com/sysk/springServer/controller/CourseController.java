package br.com.sysk.springServer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sysk.springServer.model.Course;
import br.com.sysk.springServer.service.CourseService;

@RestController
public class CourseController {

	@Autowired
	private CourseService courseService;
	
	@RequestMapping(value = "/course/list", method = RequestMethod.GET)
	public Page<Course> listCourses(@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "order", required = false) Direction direction) {
		if (page == null){
			page = 0;
		}
		if (direction == null){
			direction = Direction.ASC;
		}
		return courseService.listAll(page, direction);
	}
	
	@RequestMapping(value = "/course/{id}", method = RequestMethod.GET)
	public Course findCourse(@PathVariable Long id) throws Exception {
		return courseService.findById(id);
	}

	@RequestMapping(value = "/course/new", method = RequestMethod.POST)
	public Course addCourse(@RequestBody @Valid Course course) {
		return courseService.save(course);
	}
	
	@RequestMapping(value = "/course/{id}", method = RequestMethod.PUT)
	public Course updateCourse(@PathVariable Long id, @RequestBody @Valid Course course) throws Exception {
		return courseService.update(id, course);
	}
}
