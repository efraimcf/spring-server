package br.com.sysk.springServer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sysk.springServer.domain.HealthCheckResponse;
import br.com.sysk.springServer.domain.enums.ServiceStatus;
import br.com.sysk.springServer.service.HealthcheckService;

/**
 * Controller do Status do serviço. Utilizado principalmente pelo Load Balance
 * 
 * @author Efraim Coutinho
 */
@RestController
public class HealthcheckController {

	@Autowired
	private HealthcheckService service;
	
	@RequestMapping("/healthcheck")
	public ResponseEntity<ServiceStatus> simpleHealthCheck() throws Exception{
		HealthCheckResponse res = service.check();
		HttpStatus status = getStatus(res);
		return new ResponseEntity<ServiceStatus>(res.getStatus(), status);
	}

	@RequestMapping("/healthcheck/complete")
	public ResponseEntity<HealthCheckResponse> completeHealthCheck() throws Exception{
		HealthCheckResponse res = service.check();
		HttpStatus status = getStatus(res);
		return new ResponseEntity<HealthCheckResponse>(res, status);
	}
	
	private HttpStatus getStatus(HealthCheckResponse res) {
		if (ServiceStatus.LIVE.equals(res.getStatus())) return HttpStatus.OK;
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}
}
