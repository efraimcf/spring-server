package br.com.sysk.springServer.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.sysk.springServer.model.Student;
import br.com.sysk.springServer.service.StudentService;

@RestController
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value = "/student/list", method = RequestMethod.GET)
	public Page<Student> listStudents(@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "order", required = false) Direction direction) {
		if (page == null){
			page = 0;
		}
		if (direction == null){
			direction = Direction.ASC;
		}
		return studentService.listAll(page, direction);
	}
	
	@RequestMapping(value = "/student/course/{id}", method = RequestMethod.GET)
	public List<Student> listStudentByCourse(@PathVariable Long courseId) {
		return studentService.findByCourse(courseId);
	}

	@RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
	public Student findStudent(@PathVariable Long id) throws Exception {
		return studentService.findById(id);
	}

	@RequestMapping(value = "/student/new", method = RequestMethod.POST)
	public Student addStudent(@RequestBody @Valid Student student) {
		return studentService.save(student);
	}
	
	@RequestMapping(value = "/student/{id}", method = RequestMethod.POST)
	public Student updateStudent(@PathVariable Long id, @RequestBody @Valid Student student) throws Exception {
		return studentService.update(id, student);
	}

	@RequestMapping(value = "/student/{id}/photo", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void addStudentPhoto(@PathVariable Long id,
			@RequestParam("file") MultipartFile file) throws NumberFormatException, Exception {
		studentService.addPhoto(id, file);
	}
	
	@RequestMapping(value = "/student/{id}/photo", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> getPicture(@PathVariable Long id) throws NumberFormatException, Exception {
		File file = studentService.getPhoto(id);
		InputStream is = new FileInputStream(file);
		Path path = Paths.get(file.getAbsolutePath());
		return ResponseEntity.ok()
				.contentLength(file.length())
				.contentType(MediaType.parseMediaType(Files.probeContentType(path)))
				.body(new InputStreamResource(is));
	}
	
}
