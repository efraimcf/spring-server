package br.com.sysk.springServer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sysk.springServer.model.Teacher;
import br.com.sysk.springServer.service.TeacherService;

@RestController
public class TeacherController {

	@Autowired
	private TeacherService teacherService;
	
	@RequestMapping(value = "/teacher/list", method = RequestMethod.GET)
	public List<Teacher> listTeachers(@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "order", required = false) Direction direction) {
		if (page == null){
			page = 0;
		}
		if (direction == null){
			direction = Direction.ASC;
		}
		return teacherService.listAll(page, direction);
	}
	
	@RequestMapping(value = "/teacher/course/{id}", method = RequestMethod.GET)
	public Teacher listTeacherByCourse(@PathVariable Long courseId) {
		return teacherService.findByCourse(courseId);
	}

	@RequestMapping(value = "/teacher/{id}", method = RequestMethod.GET)
	public Teacher findTeacher(@PathVariable Long id) throws Exception {
		return teacherService.findById(id);
	}

	@RequestMapping(value = "/teacher/new", method = RequestMethod.POST)
	public Teacher addTeacher(@RequestBody @Valid Teacher teacher) {
		return teacherService.save(teacher);
	}
	
	@RequestMapping(value = "/teacher/{id}", method = RequestMethod.PUT)
	public Teacher updateCourse(@PathVariable Long id, @RequestBody @Valid Teacher course) throws Exception {
		return teacherService.update(id, course);
	}

}
