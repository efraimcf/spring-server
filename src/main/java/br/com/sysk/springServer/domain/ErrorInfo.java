package br.com.sysk.springServer.domain;

public class ErrorInfo {
    public final String url;
    public final String errorMessage;

    public ErrorInfo(String url, Exception e) {
        this.url = url;
        this.errorMessage = e.getMessage();
    }

    public ErrorInfo(String url, String message) {
        this.url = url;
        this.errorMessage = message;
    }
}
