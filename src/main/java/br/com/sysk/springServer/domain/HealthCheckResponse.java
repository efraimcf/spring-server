package br.com.sysk.springServer.domain;

import java.util.LinkedList;
import java.util.List;

import br.com.sysk.springServer.domain.enums.ServiceStatus;

/**
 * Healthcheck object response
 * @author Efraim Coutinho Ferreira 
 */
public class HealthCheckResponse {

	private String version;
	
	private String name;
	
	private ServiceStatus status;
	
	private List<ServiceInfo> services;

	public HealthCheckResponse() {}
	
	public HealthCheckResponse(String name, String version, ServiceStatus status) {
		this.name = name;
		this.version = version;
		this.status = status;
		this.services = new LinkedList<ServiceInfo>();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServiceStatus getStatus() {
		return status;
	}

	public void setStatus(ServiceStatus status) {
		this.status = status;
	}

	public List<ServiceInfo> getServices() {
		return services;
	}

	public void setServices(List<ServiceInfo> services) {
		this.services = services;
	}
}
