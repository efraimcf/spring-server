package br.com.sysk.springServer.domain;

import br.com.sysk.springServer.domain.enums.ServiceStatus;

public class ServiceInfo {

	private String name;
	
	private String host;
	
	private ServiceStatus status;
	
	private String message;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public ServiceStatus getStatus() {
		return status;
	}

	public void setStatus(ServiceStatus status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
