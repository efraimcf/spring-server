package br.com.sysk.springServer.domain.enums;

/**
 * Status of service
 * @author Efraim Coutinho Ferreira
 *
 */
public enum ServiceStatus {

	LIVE,
	DOWN
}
