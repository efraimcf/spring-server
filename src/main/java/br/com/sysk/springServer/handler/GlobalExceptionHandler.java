package br.com.sysk.springServer.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.sysk.springServer.domain.ErrorInfo;

/**
 * Handler para tratar as Exceptions lançadas e poder enviar a mensagem apropriada
 * 
 * @author Efraim Coutinho
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ErrorInfo fileNotFound(HttpServletRequest req, Exception e){
		return new ErrorInfo(req.getRequestURL().toString(), e);
	}
}
