package br.com.sysk.springServer.handler;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.sysk.springServer.domain.ErrorInfo;

@Controller
public class HttpErrorHandler implements ErrorController {

	private static final String PATH = "/error";
	private static final String UNAUTHORIZED_MESSAGE = "Access Denied.";
	private static final String NOT_FOUND_MESSAGE = "No resource found.";
	private static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal server error.";

	@Override
	public String getErrorPath() {
		return PATH;
	}
	
	@RequestMapping(PATH)
	public ResponseEntity<ErrorInfo> error(@RequestParam("code") String code, HttpServletRequest req) throws Exception {
		ErrorInfo error = null;
		BodyBuilder builder = null;
		if ("401".equals(code)) {
			error = new ErrorInfo(req.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI).toString(), UNAUTHORIZED_MESSAGE);
			builder = ResponseEntity.status(HttpStatus.UNAUTHORIZED);
		}else if ("404".equals(code)) {
			error = new ErrorInfo(req.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI).toString(), NOT_FOUND_MESSAGE);
			builder = ResponseEntity.status(HttpStatus.NOT_FOUND);
		}else{
    		error = new ErrorInfo(req.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI).toString(), INTERNAL_SERVER_ERROR_MESSAGE);
    		builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		return builder.body(error);
	}
}
