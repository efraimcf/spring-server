package br.com.sysk.springServer.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.sysk.springServer.model.util.DomainModel;

@Entity
@Table(name = Student.TABLE_NAME)
public class Student extends DomainModel {
	
	private static final long serialVersionUID = 4775978671972681275L;

	public static final String TABLE_NAME = "student";
	
	@NotNull
	private String name;
	
	@NotNull
	private Integer age;
	
	@NotNull
	@Pattern(regexp="^[0-9]{11}$", message="Invalid data")
	private String document;
	
	@JsonIgnore
	private String photo;
	
	@ManyToMany(mappedBy="students", fetch = FetchType.LAZY)
	private List<Course> courses;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}
	
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<Course> getCourses() {
		return courses;
	}
	
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
