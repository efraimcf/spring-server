package br.com.sysk.springServer.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.sysk.springServer.model.util.DomainModel;

@Entity
@Table(name = Teacher.TABLE_NAME)
public class Teacher extends DomainModel {

	private static final long serialVersionUID = 4691051653347402427L;

	public static final String TABLE_NAME = "teacher";

	@NotNull
	private String name;
	
	@OneToMany(mappedBy = "teacher", fetch = FetchType.LAZY)
	@Column(name = "course_id")
	private List<Course> courses;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
