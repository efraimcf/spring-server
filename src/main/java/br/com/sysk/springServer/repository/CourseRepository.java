package br.com.sysk.springServer.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.sysk.springServer.model.Course;

public interface CourseRepository extends JpaRepository<Course, Long>{

	@Query("SELECT c FROM Course c")
	Page<Course> findAll(Pageable pageable);
}
