package br.com.sysk.springServer.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.sysk.springServer.model.Student;

public interface StudentRepository extends PagingAndSortingRepository<Student, Long>{
	
	Page<Student> findAll(Pageable pageable);
	
	@Query("SELECT s FROM Student s INNER JOIN s.courses c WHERE c.id = ?1")
	List<Student> findByCourseId(Long courseId);
	
	
}
