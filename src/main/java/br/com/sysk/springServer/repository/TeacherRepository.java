package br.com.sysk.springServer.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.sysk.springServer.model.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, Long>{

	@Query("SELECT t FROM Teacher t")
	List<Teacher> findAll(Pageable pageable);
	
	@Query("SELECT DISTINCT t FROM Teacher t INNER JOIN t.courses c WHERE c.id = ?1")
	Teacher findByCourseId(Long courseId);
}
