package br.com.sysk.springServer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.sysk.springServer.model.Course;
import br.com.sysk.springServer.repository.CourseRepository;

@Service
public class CourseService {

	private static final String FIELD = "description";
	
	@Value("${files.path}")
	private String path; 
	
	@Value("${response.size}")
	private Integer size;
	
	@Autowired
	private CourseRepository courseRepository;

	public Page<Course> listAll(int page, Direction direction){
		return courseRepository.findAll(new PageRequest(page, size, direction, FIELD));
	}

	public Course findById(Long id) throws Exception {
		Course course = courseRepository.findOne(id);
		if (course == null) {
			throw new Exception("Course not found");
		}
		return course;
	}

	public Course save(Course course) {
		return courseRepository.save(course);
	}

	public Course update(Long id, Course course) throws Exception {
		findById(id);
		return save(course);
	}
}
