package br.com.sysk.springServer.service;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.sysk.springServer.domain.HealthCheckResponse;
import br.com.sysk.springServer.domain.ServiceInfo;
import br.com.sysk.springServer.domain.enums.ServiceStatus;

/**
 * Health check service
 * @author Efraim Coutinho Ferreira
 */
@Service
public class HealthcheckService {

	public static final Logger LOGGER = LoggerFactory.getLogger(HealthcheckService.class);
	private static final String LIVE_MESSAGE = "Service is OK";
	
	@Value("${app.version}")
	private String appVersion;
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${db.service.name}")
	private String dbServiceName;

	@Value("${db.url}")
	private String dbHost;

	@Value("${db.select.test}")
	private String query;
	
	@Autowired
	private EntityManager manager;
	
	/**
	 * Testa o status de todos as serviços utilizados pelo sistema
	 * @return
	 * @throws Exception
	 */
	public HealthCheckResponse check() throws Exception{
		HealthCheckResponse res = new HealthCheckResponse(appName, appVersion, ServiceStatus.LIVE);
		res.getServices().add(checkDatabase());
		for (ServiceInfo service : res.getServices()){
			if (ServiceStatus.DOWN.equals(service.getStatus())){
				res.setStatus(ServiceStatus.DOWN);
			}
		}
		return res;
	}
	
	private ServiceInfo checkDatabase() {
		ServiceInfo service = new ServiceInfo();
		try{
			manager.createNativeQuery(query).getFirstResult();
			service.setName(dbServiceName);
			service.setHost(dbHost);
			service.setStatus(ServiceStatus.LIVE);
			service.setMessage(LIVE_MESSAGE);
		}catch(Exception e){
			service.setStatus(ServiceStatus.LIVE);
			service.setMessage(getCause(e));
			LOGGER.error("Database connection is down.", e);
		}
		return service;
	}
	
//	/**
//	 * Testando comunicação com serviços externos
//	 * @param name
//	 * @param host
//	 * @return
//	 */
//	private boolean testExternalRestService(String name, String host) {
//		try { 
//			URL endpoint = new URL(host);
//			HttpURLConnection connection = (HttpURLConnection) endpoint.openConnection();
//			connection.setRequestMethod("GET");
//			connection.connect();
//			
//			int code = connection.getResponseCode();
//			if (code == 200) {
//				LOGGER.debug("Host [{}] is alive", host);
//				return true;
//			}
//			throw new Exception();
//		}catch(Exception e){
//			LOGGER.error("Host [{}] is down", host);			
//			return false;
//		}
//	}

	/**
	 * Resgatando a mensagem original da Exception
	 * @param e
	 * @return
	 */
	private String getCause(Throwable e){
		if (e.getCause() != null) {
			String message = getCause(e.getCause()); 
			return message == null? e.getMessage() : message;
		}
		return e.getMessage();
	}

}
