package br.com.sysk.springServer.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sysk.springServer.model.Student;
import br.com.sysk.springServer.repository.StudentRepository;

@Service
public class StudentService {

	private static final String FIELD = "name";
	
	@Value("${files.path}")
	private String path; 
	
	@Value("${response.size}")
	private Integer size;
	
	@Autowired
	private StudentRepository studentRepository;

	public Page<Student> listAll(int page, Direction direction){
		return studentRepository.findAll(new PageRequest(page, size, direction, FIELD));
	}
	
	public Student findById(Long id) throws Exception {
		Student student = findById(id); 
		if (student == null) {
			throw new Exception("Student not found");
		}
		return student;
	}

	public List<Student> findByCourse(Long courseId) {
		return studentRepository.findByCourseId(courseId);
	}

	public Student save(Student student) {
		return studentRepository.save(student);
	}

	public Student update(Long id, Student student) throws Exception {
		findById(id);
		return studentRepository.save(student);
	}

	public void addPhoto(Long id, MultipartFile file) throws Exception {
		if (file.isEmpty()) {
            throw new Exception("Failed to store empty file " + file.getOriginalFilename());
        }
		Student student = findById(id);
		if (student == null){
			throw new Exception("Student not found");
		}
		Path location = Paths.get(path).resolve(file.getOriginalFilename());
		Files.copy(file.getInputStream(), location);
		student.setPhoto(location.toString());
		save(student);
	}

	public File getPhoto(Long id) throws Exception {
		Student student = findById(id);
		if (student == null){
			throw new Exception("Student not found");
		}
		File file = new File(student.getPhoto());
		if (!file.exists()){
			throw new Exception("Photo not found");
		}
		return file;
	}
}
