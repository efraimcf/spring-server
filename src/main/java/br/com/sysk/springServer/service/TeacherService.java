package br.com.sysk.springServer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.sysk.springServer.model.Teacher;
import br.com.sysk.springServer.repository.TeacherRepository;

@Service
public class TeacherService {

	private static final String FIELD = "name";
	
	@Value("${files.path}")
	private String path; 
	
	@Value("${response.size}")
	private Integer size;
	
	@Autowired
	private TeacherRepository teacherRepository;

	public List<Teacher> listAll(int page, Direction direction){
		return teacherRepository.findAll(new PageRequest(page, size, direction, FIELD));
	}

	public Teacher findById(Long id) throws Exception {
		Teacher teacher = teacherRepository.findOne(id);
		if (teacher == null) {
			throw new Exception("Teacher not found");
		}
		return teacher;
	}

	public Teacher findByCourse(Long courseId) {
		return teacherRepository.findByCourseId(courseId);
	}

	public Teacher save(Teacher teacher) {
		return teacherRepository.save(teacher);
	}

	public Teacher update(Long id, Teacher teacher) throws Exception {
		findById(id);
		return save(teacher);
	}
}
